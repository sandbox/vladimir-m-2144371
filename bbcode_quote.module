<?php
/**
 * @file
 * Main file for bbcode_quote module.
 */

/**
 * Implements hook_filter_info().
 */
function bbcode_quote_filter_info() {
  $filters['bbcode_quote'] = array(
    'title' => t('Convert BBcode + Quote to HTML'),
    'process callback' => 'bbcode_quote_filter_bbcode_quote_process',
    'settings callback' => 'bbcode_quote_filter_bbcode_quote_settings',
    'cache' => FALSE,
  );
  return $filters;
}

/**
 * Implements hook_filter_FILTER_process().
 */
function bbcode_quote_filter_bbcode_quote_process($text, $filter, $format) {
  return bbcode_quote_filter_process($text, $filter->settings, $format);
}

/**
 * Implements hook_filter_FILTER_settings().
 */
function bbcode_quote_filter_bbcode_quote_settings($form, &$form_state, $filter, $format, $defaults, $filters) {
  $filter->settings += $defaults;
  $form = array();
  $options = bbcode_quote_filters_alter();
  foreach ($options as $key => $value) {
    $list[$key] = $key . ' [' . $value['pattern'] . ']';
  }
  $form['bbcode_quote'] = array(
    '#type' => 'item',
    '#title' => t('Available configuration(s)'),
    '#markup' => theme('item_list', array('items' => $list)),
  );

  return $form;
}

/**
 * Implements hook_bbcode_quote_filters_alter().
 */
function bbcode_quote_filters_alter() {
  $options['bbcode_quote'] = array(
    'pattern' => '/\[quote:([^=]*)="([^"]*)"](.*)\[\/quote\]/',
    'replacement' => array(
      'html' => '<div class="bb-quote">$2 wrote:<blockquote class="bb-quote-body">$3</blockquote></div>',
      'callback' => NULL,
    ),
  );
  $options['convert_double_less_than_sign'] = array(
    'pattern' => '/(<<)/',
    'replacement' => array(
      'html' => '<<',
      'callback' => 'bbcode_quote_convert_double_less_than_sign',
    ),
  );
  foreach (module_implements('bbcode_quote_filters_alter') as $module) {
    $function = $module . '_bbcode_quote_filters_alter';
    $function($options);
  }
  drupal_alter('bbcode_quote_filters_alter', $options);

  return $options;
}

/**
 * Filter process callback.
 */
function bbcode_quote_filter_process($text, $filter, $format) {
  $options = bbcode_quote_filters_alter();
  if (!empty($options) && is_array($options)) {
    foreach ($options as $values) {
      if (empty($values['replacement'])) {
        watchdog('bbcode_quote_filter_process', 'No replacement provided.');
        return;
      }
      else {
        $replacement = $values['replacement'];
      }
      if (empty($values['pattern'])) {
        watchdog('bbcode_quote_filter_process', 'No pattern provided.');
        return;
      }
      else {
        $pattern = $values['pattern'];
        if (is_array($replacement)) {
          if (!empty($replacement['callback'])) {
            if (function_exists($replacement['callback'])) {
              $function = $replacement['callback'];
              $text = $function($pattern, $replacement['html'], $text);
            }
            else {
              drupal_set_message(t('Function @name was not found.', array('@name' => check_plain($replacement['callback']) . '()')), 'error', FALSE);
            }
          }
          else {
            $text = preg_replace($pattern, $replacement['html'], $text);
          }
        }
      }
    }
  }
  return $text;
}

/**
 * Replacement callback.
 */
function bbcode_quote_convert_double_less_than_sign($pattern, $replacement, $string) {
  $string = preg_replace($pattern, htmlentities($replacement), $string);
  return $string;
}
