BBCodes + Quotes Module.

The Drupal bbcode_quote.module adds a BBCode + Quote filter to Drupal.
This allows you to use HTML-like tags as an alternative to HTML itself for
adding markup to your posts.

BBCode + Quote is easier to use than HTML and helps to prevent malicious users
from disrupting your site's formatting.


Installation
============
  - Download the BBCodes + Quotes module
  - Create a .../modules/bbcode_quote/ subdirectory and copy the files into it.
  - Enable the module as usual from Drupal's admin pages
    (Administer » Modules)


Configuration
=============
  - Before using BBCodes + Quotes you need to enable the BBCode filter in an
    input format (see Administer » Input formats » add input format)
  - You can enable/ disable the BBCodes + Quotes module in the configuration
    page of the input format.
  - BBCodes + Quotes provide hook_bbcode_quote_filters_alter() function.
    For more details look in bbcode_quote.api.php file.
